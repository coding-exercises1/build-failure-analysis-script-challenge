def parse_job_data(file_path):
    job_data = []
    with open(file_path, 'r') as file:
        for line in file:
            job_name, build_number, result = line.strip().split()
            build_number = int(build_number)
            job_data.append((job_name, build_number, result))
    return job_data

def longest_consecutive_failures(job_data):
    longest_failures = {}
    sorted_data = sorted(job_data, key=lambda x: (x[0], x[1]))
    
    current_job = None
    current_failure_count =  0
    
    for job_name, build_number, result in sorted_data:
        if job_name != current_job:
            if current_job is not None and current_failure_count > longest_failures.get(current_job,  0):
                longest_failures[current_job] = current_failure_count
            current_job = job_name
            current_failure_count =  0 if result == "SUCCESS" else  1
        elif result == "FAILED":
            current_failure_count +=  1
        else:
            if current_failure_count > longest_failures.get(current_job,  0):
                longest_failures[current_job] = current_failure_count
            current_failure_count =  0
    
    # Check for the last job's longest consecutive failures
    if current_failure_count > longest_failures.get(current_job,  0):
        longest_failures[current_job] = current_failure_count
    
    return longest_failures

def main():
    job_data = parse_job_data("data.txt")
    longest_failures = longest_consecutive_failures(job_data)
    
    for job, count in longest_failures.items():
        print(f"{job} had {count} consecutive build failures")

if __name__ == "__main__":
    main()
